﻿CREATE TABLE [dbo].[Mascota]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Nombre] TEXT NOT NULL, 
    [Edad] TEXT NOT NULL, 
    [Color] TEXT NOT NULL, 
    [Especie] TEXT NOT NULL, 
    [Raza] TEXT NOT NULL
)
