﻿CREATE TABLE [dbo].[MascotaSet] (
    [IdMascota] INT            IDENTITY (1, 1) NOT NULL,
    [Nombre]    TEXT NOT NULL,
    [Color]     TEXT NOT NULL,
    [Edad]      TEXT NOT NULL,
    [Especie]   TEXT NOT NULL,
    [Raza]      TEXT NOT NULL,
    CONSTRAINT [PK_MascotaSet] PRIMARY KEY CLUSTERED ([IdMascota] ASC)
);

