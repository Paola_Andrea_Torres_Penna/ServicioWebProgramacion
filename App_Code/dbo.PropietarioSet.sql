﻿CREATE TABLE [dbo].[PropietarioSet] (
    [IdPropietario]  INT            IDENTITY (1, 1) NOT NULL,
    [Nombre]         NVARCHAR (MAX) NOT NULL,
    [Identificacion] NVARCHAR (MAX) NOT NULL,
    [Edad]           NVARCHAR (MAX) NOT NULL,
    [Correo]         NVARCHAR (MAX) NOT NULL,
    [Direccion]      NVARCHAR (MAX) NOT NULL,
    [Telefono]       NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_PropietarioSet] PRIMARY KEY CLUSTERED ([IdPropietario] ASC)
);

