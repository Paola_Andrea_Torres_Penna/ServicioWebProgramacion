
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/27/2015 00:41:01
-- Generated from EDMX file: d:\Documents\Visual Studio 2013\WebSites\Webproyectoclinicav\App_Code\Veterinaria.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Veterinaria];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[MascotaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MascotaSet];
GO
IF OBJECT_ID(N'[dbo].[PropietarioSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropietarioSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'MascotaSet'
CREATE TABLE [dbo].[MascotaSet] (
    [IdMascota] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Color] nvarchar(max)  NOT NULL,
    [Edad] nvarchar(max)  NOT NULL,
    [Especie] nvarchar(max)  NOT NULL,
    [Raza] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PropietarioSet'
CREATE TABLE [dbo].[PropietarioSet] (
    [IdPropietario] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Identificacion] nvarchar(max)  NOT NULL,
    [Edad] nvarchar(max)  NOT NULL,
    [Correo] nvarchar(max)  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [IdMascota] in table 'MascotaSet'
ALTER TABLE [dbo].[MascotaSet]
ADD CONSTRAINT [PK_MascotaSet]
    PRIMARY KEY CLUSTERED ([IdMascota] ASC);
GO

-- Creating primary key on [IdPropietario] in table 'PropietarioSet'
ALTER TABLE [dbo].[PropietarioSet]
ADD CONSTRAINT [PK_PropietarioSet]
    PRIMARY KEY CLUSTERED ([IdPropietario] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------